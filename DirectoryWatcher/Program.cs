﻿using System;
using System.IO;
using System.Security.Permissions;

namespace DirectoryWatcher {
	class Program {

		private static void PrintHelpAndExit(int code = 0) {
			
			Console.Error.WriteLine("DirectoryWatcher.exe -d directory [-f *.txt] [-n created] [-r]");
			Console.Error.WriteLine("   -d / --directory : directory to monitor");
			Console.Error.WriteLine("   -f / --format    : file format to monitor              (default: *)");
			Console.Error.WriteLine("   -n / --notify    : created, changed, deleted, renamed  (default: everything.)");
			Console.Error.WriteLine("                      [ex) -n created -n changed ...]");
			Console.Error.WriteLine("   -r / --recursion : include subdirecoties to monitor");
			Console.Error.WriteLine("");

			Environment.Exit(code);
		}

		private enum Notify {
			CREATED = 0,
			CHANGED = 1,
			DELETED = 2,
			RENAMED = 3
		};

		public static void Main(string[] args) {
			if(args.Length < 1) {
				PrintHelpAndExit(0);
			}

			DirectoryInfo dir = null;
			string format = null;
			bool[] notify = null;
			bool recursion = false;

			// argument parse
			try {
				for(int i = 0; i < args.Length; i++) {
					switch(args[i].ToLower()) {
						case "-d":
						case "--directory":
							if(dir == null) {
								dir = new DirectoryInfo(args[++i]);
								if(!dir.Exists) {
									Console.WriteLine("no exists directory\n");
									PrintHelpAndExit(0);
								}
							}
							else {
								PrintHelpAndExit(0);
							}
							break;
						case "-f":
						case "--format":
							if(format == null) {
								format = args[++i];
							}
							else {
								PrintHelpAndExit(0);
							}
							break;
						case "-n":
						case "--notify":
							if(notify == null) {
								notify = new bool[4];
							}
							string val = args[++i].ToUpper();
							int __idx = (int)Enum.Parse(typeof(Notify), val);
							notify[__idx] = true;
							break;
						case "-r":
						case "--recursion":
							if(recursion) {
								PrintHelpAndExit(0);
							}
							recursion = true;
							break;
					}
				}
			}
			catch(Exception) {
				PrintHelpAndExit(0);
			}
			// argument parse End

			if(dir == null) {
				PrintHelpAndExit(0);
			}
			if(format == null) {
				format = "*";
			}
			if(notify == null) {
				notify = new bool[4] { true, true, true, true };
				notify[(int)Program.Notify.CREATED] = true;
			}

			Run(dir.FullName, format, notify, recursion);
		}

		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
		public static void Run(string directory, string format, bool[] notify, bool recursion) {
			FileSystemWatcher watcher = new FileSystemWatcher();
			// 감시 할 최상위 경로
			watcher.Path = directory;
			// 파일 필터 ( * , *.* , *.txt , *.png ... )
			watcher.Filter = format;
			// 하위 폴더도 알릴지
			watcher.IncludeSubdirectories = recursion;

			watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
			   | NotifyFilters.FileName | NotifyFilters.DirectoryName;

			// 이벤트 추가
			if(notify[(int)Notify.CHANGED]) {
				watcher.Changed += new FileSystemEventHandler(OnChanged);
			}
			if(notify[(int)Notify.CREATED]) {
				watcher.Created += new FileSystemEventHandler(OnChanged);
			}
			if(notify[(int)Notify.DELETED]) {
				watcher.Deleted += new FileSystemEventHandler(OnChanged);
			}
			if(notify[(int)Notify.RENAMED]) {
				watcher.Renamed += new RenamedEventHandler(OnRenamed);
			}

			watcher.EnableRaisingEvents = true;

			while(true) ;
		}

		private static void OnChanged(object source, FileSystemEventArgs e) {
			// 파일 생성, 변화, 삭제 시 이벤트 출력
			// created|"file name"
			// changed|"file name"
			// deleted|"file name"
			Console.WriteLine("{0}|\"{1}\"",
				e.ChangeType.ToString().ToLower(), e.FullPath);
		}

		private static void OnRenamed(object source, RenamedEventArgs e) {
			// 파일의 이름이 변경된 경우
			// renamed|"old name"|"new name"
			Console.WriteLine("{0}|{1}|{2}",
				e.ChangeType.ToString().ToLower(), e.OldFullPath, e.FullPath);
		}
	}
}
